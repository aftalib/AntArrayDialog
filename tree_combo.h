#ifndef TREE_COMBO_H
#define TREE_COMBO_H

#include <QObject>
#include <QComboBox>
#include <QTreeView>
#include <QEvent>
#include <QMouseEvent>
#include <QDebug>

class TreeCombo : public QComboBox //Un clic droit sur QComboBox puis Refactor puis Insert Virtual Functions
                              //donne la liste des fonctions virtuelles disponibles
{
    Q_OBJECT
public:
    explicit TreeCombo(QWidget *parent = nullptr);

signals:
    void currentItemChanged(QString text);

private:
    QTreeView *m_tree = new QTreeView;
    bool m_hide = true;
    QString m_currentFile;


    // QObject interface
public:
    bool eventFilter(QObject *watched, QEvent *event);

    // QComboBox interface
public:
    void showPopup();
    void hidePopup();
};

#endif // TREE_COMBO_H
