#include "spinboxdelegate.h"

SpinBoxDelegate::SpinBoxDelegate(QObject *parent, double valMin, double valMax) :
    QStyledItemDelegate (parent),
    m_valMin(valMin), m_valMax(valMax)
{

}

QWidget *SpinBoxDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);

    QDoubleSpinBox *editor = new QDoubleSpinBox(parent);
    editor->setMinimum(m_valMin);
    editor->setMaximum(m_valMax);
    editor->setDecimals(4);
    editor->setAlignment(Qt::AlignHCenter);
    return editor;
}

//Sets the contents of the given editor to the data for the item at the given index
void SpinBoxDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    // Get the value via index of the Model
    double value = index.model()->data(index, Qt::EditRole).toDouble();

    // Put the value into the SpinBox
    QDoubleSpinBox *spinbox = static_cast<QDoubleSpinBox*>(editor);
    spinbox->setValue(value);
}

//Gets data from the editor widget and stores it in the specified model
//at the item index.
void SpinBoxDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QDoubleSpinBox *spinbox = static_cast<QDoubleSpinBox*>(editor);
    spinbox->interpretText();
    //This function interprets the text of the spin box.
    //If the value has changed since last interpretation it will emit signals.

    //Update the model
    double value = spinbox->value();
    model->setData(index, value, Qt::EditRole);
}

//Updates the editor for the item specified by index according
//to the style option given.
void SpinBoxDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry(option.rect);
}
