#ifndef ANTENNAARRAY_H
#define ANTENNAARRAY_H

#include <QObject>
#include <QDir>
#include <QDebug>

class AntennaArray : public QObject
{
    Q_OBJECT
public:
    explicit AntennaArray(QObject *parent = nullptr, QString path="");

    bool loadFile(QString path);
    bool saveFile(QString path);
    int getNbOfAnt()
    {
        return m_nbOfAnt;
    }

    QVector<double> getAmpCSB_CRS(){return m_ampCSB_CRS;}
    QVector<double> getPhCSB_CRS(){return m_phCSB_CRS;}
    QVector<double> getAmpSBO_CRS(){return m_ampSBO_CRS;}
    QVector<double> getPhSBO_CRS(){return m_phSBO_CRS;}
    QVector<double> getAmpAntError(){return m_ampAntError;}
    QVector<double> getPhAntError(){return m_phAntError;}
    QVector<double> getAmpCoupling(){return m_ampCoupling;}
    QVector<double> getPhCoupling(){return m_phCoupling;}
    QVector<double> getAmpCSB_CLR(){return m_ampCSB_CLR;}
    QVector<double> getPhCSB_CLR(){return m_phCSB_CLR;}
    QVector<double> getAmpSBO_CLR(){return m_ampSBO_CLR;}
    QVector<double> getPhSBO_CLR(){return m_phSBO_CLR;}
    QVector<double> getantPos_X(){return m_antPosErrorX;}
    QVector<double> getantPos_Y(){return m_antPosErrorY;}
    QVector<double> getantPos_Z(){return m_antPosErrorZ;}

    int getArrayType(){return m_arrayType;}

signals:



private:

    void readArrayData(QString &text, QString label, int &indexEnd, QVector<int> *array);
    void readArrayData(QString &text, QString label, int &indexEnd, QVector<double> *array);
    void readSingleData(QString &text, QString label, QString endChar, int &indexEnd, int &data);
    void readSingleData(QString &text, QString label, QString endChar, int &indexEnd, double &data);

    void writeSingleData (QByteArray ba, int &var, int labelWidth, int dataWidth,
                          QFile &file, QByteArray endText="");
    void writeSingleData (QByteArray ba, double &var, int labelWidth, int dataWidth,
                          int nbOfDeci, QFile &file, QByteArray endText="");
    void writeArrayData (QByteArray ba, QVector<double> &array, int nbOfDeci, QFile &file);


    QString m_arrayName;
    int m_nbOfAnt;
    int m_arrayType;
    double m_antHeights[2];
    int m_antType[2];
    int m_spacingType;
    double m_baseSpacing;
    double m_designFreq;
    int m_spacingUnit;
    QVector<int> m_heightBtState;
    QVector<double> m_spacing;
    QVector<double> m_antPosErrorX;
    QVector<double> m_antPosErrorY;
    QVector<double> m_antPosErrorZ;
    double m_arrayRot;
    double m_arrayY_Offset;

    QVector<double> m_ampCSB_CRS;
    QVector<double> m_phCSB_CRS;
    QVector<double> m_ampSBO_CRS;
    QVector<double> m_phSBO_CRS;

    QVector<double> m_ampCSB_CLR;
    QVector<double> m_phCSB_CLR;
    QVector<double> m_ampSBO_CLR;
    QVector<double> m_phSBO_CLR;

    QVector<double> m_ampCSB_CLR2;
    QVector<double> m_phCSB_CLR2;
    QVector<double> m_ampSBO_CLR2;
    QVector<double> m_phSBO_CLR2;

    int m_CLR2_On;

    QVector<double> m_ampAntError;
    QVector<double> m_phAntError;
    QVector<double> m_ampCoupling;
    QVector<double> m_phCoupling;

    int m_defaultSigGen;
    double m_defaultCRS_Power;
    double m_defaultCLR_Power;
    double m_CLR_CRS_Ratio;
    double m_CLR_Az;
    double m_CLR_DDMatAz;
    int m_photoIndex;



};

#endif // ANTENNAARRAY_H
