#include "antarraydialog.h"

#include <QApplication>
#include <QStyleFactory>
#include <QPixmap>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setStyle(QStyleFactory::create("Fusion"));


    AntArrayDialog w;
    w.show();
    return a.exec();
}
