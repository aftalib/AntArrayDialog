#ifndef ANTARRAYDIALOG_H
#define ANTARRAYDIALOG_H

#include <QDialog>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QFileInfo>
#include <QDir>
#include <QScrollBar>
#include "antennaarray.h"
#include "spinboxdelegate.h"
#include "math.h"
#include "qapplication.h"

QT_BEGIN_NAMESPACE
namespace Ui { class AntArrayDialog; }
QT_END_NAMESPACE

class AntArrayDialog : public QDialog {
    Q_OBJECT

public:
    AntArrayDialog(QWidget *parent = nullptr);
    ~AntArrayDialog();

private:
    void myInit();
    void addFiles(QStandardItem *root, QFileInfo dir);
    void setFileName(QString text);

    Ui::AntArrayDialog *ui;
    SpinBoxDelegate *m_customSpinBox;
    QStandardItemModel m_antArrayFilesModel;

    QStandardItemModel *m_feedCRS_Model;

    //Fenêtre ANTENNA SETTINGS

    QStandardItemModel *m_table_right_pos;
    QStandardItemModel *m_table_left_pos;
    QStandardItemModel *m_table_space_pos;
    QStandardItemModel *m_table_imp_pos;

    // Fenêtres COURSES
    QStandardItemModel *m_CSB_CRS_LeftModel;
    QStandardItemModel *m_CSB_CRS_RightModel;
    QStandardItemModel *m_SBO_CRS_LeftModel;
    QStandardItemModel *m_SBO_CRS_RightModel;
    QStandardItemModel *m_table_imp_up;
    QStandardItemModel *m_table_imp_up2;

    // Fenêtres CLEARANCE

    QStandardItemModel *m_CSB_CLR_LeftModel;
    QStandardItemModel *m_CSB_CLR_RightModel;
    QStandardItemModel *m_SBO_CLR_LeftModel;
    QStandardItemModel *m_SBO_CLR_RightModel;
    QStandardItemModel *m_CSB_CLR_imp;
    QStandardItemModel *m_SBO_CLR_imp;


    //Fenêtre ANTENNA ERRORS
    QStandardItemModel *m_table_err_left;
    QStandardItemModel *m_table_err_right;
    QStandardItemModel *m_table_imp;


    //Fenêtre MONITORS
    QStandardItemModel *m_table_moni;
    QStandardItemModel *m_table_moni_imp;

    //Fenêtre COUPLING
    QStandardItemModel *m_table_left_coupling;
    QStandardItemModel *m_table_right_coupling;
    QStandardItemModel *m_table_imp_coupling;


    AntennaArray antArray;
    QString m_antArrayDir;
    QString m_antArrayDir2;
    int m_nbOfCols=4;
    int m_colWidth;
    int m_vHeaderWidth;
    int m_tableWidth;

    // QWidget interface

signals:
    void changed();

public slots :
    void on_btn_off_clicked();
    void on_btn_on_clicked();
    void on_reset_ph_clicked();



protected:
   // void resizeEvent(QResizeEvent *event);
private slots:
    void on_db_clicked();

};
#endif // ANTARRAYDIALOG_H
