#include "antarraydialog.h"
#include "ui_antarraydialog.h"

AntArrayDialog::AntArrayDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::AntArrayDialog)
{
    ui->setupUi(this);
    myInit();


}

//  Thales 20_10, Thales 21_7

AntArrayDialog::~AntArrayDialog()
{
    delete ui;
}

void AntArrayDialog::myInit()
{
    QString path1 = QDir::currentPath();

    int index = path1.lastIndexOf("/");
    //Qt utilise par défaut "/" comme séparateur de répertoire.
    //Sous window c'est automatiquement converti en "\" pour accéder aux fichiers

    //Construction du chemin au se trouve les réseaux d'antennes
    m_antArrayDir = path1.mid(0, index);//Retourne n (index) caractères à partir de
    //l'index 0
    m_antArrayDir.append("/Build in Arrays");



    QDir dir2(m_antArrayDir);
    QFileInfoList list = dir2.entryInfoList();

    foreach(QFileInfo info, list)
    {
        if (info.isDir() && info.fileName()!="." && info.fileName()!="..")
        {   //Si l'élément de la liste est un répertoire
            //et si ce n'est pas un des répertoire caché . ou ..
            addFiles(m_antArrayFilesModel.invisibleRootItem(), info);
        }
    }

    m_CSB_CRS_RightModel = new QStandardItemModel(this);
    m_CSB_CRS_LeftModel = new QStandardItemModel(this);
    m_SBO_CRS_RightModel = new QStandardItemModel(this);
    m_SBO_CRS_LeftModel = new QStandardItemModel(this);
    m_table_err_left = new QStandardItemModel(this);
    m_table_err_right = new QStandardItemModel(this);
    m_table_imp = new QStandardItemModel(this);
    m_table_imp_up = new QStandardItemModel(this);
    m_table_imp_up2 = new QStandardItemModel(this);
    m_customSpinBox = new SpinBoxDelegate(this);
    m_table_left_coupling = new QStandardItemModel(this);
    m_table_right_coupling = new QStandardItemModel(this);
    m_table_imp_coupling = new QStandardItemModel(this);
    m_table_moni = new QStandardItemModel(this);
    m_table_moni_imp = new QStandardItemModel(this);
    m_CSB_CLR_LeftModel = new QStandardItemModel(this);
    m_CSB_CLR_RightModel = new QStandardItemModel(this);
    m_SBO_CLR_LeftModel = new QStandardItemModel(this);
    m_SBO_CLR_RightModel = new QStandardItemModel(this);
    m_CSB_CLR_imp = new QStandardItemModel(this);
    m_SBO_CLR_imp = new QStandardItemModel(this);
    m_table_imp_pos = new QStandardItemModel(this);
    m_table_left_pos = new QStandardItemModel(this);
    m_table_right_pos = new QStandardItemModel(this);
    m_table_space_pos = new QStandardItemModel(this);


    ui->antArrayCombo->setModel(&m_antArrayFilesModel);
    ui->tableCSB_Right->setModel(m_CSB_CRS_RightModel);
    ui->tableCSB_Left->setModel(m_CSB_CRS_LeftModel);
    ui->tableSBO_Right->setModel(m_SBO_CRS_RightModel);
    ui->tableSBO_Left->setModel(m_SBO_CRS_LeftModel);
    ui->tableErr_left->setModel(m_table_err_left);
    ui->tableErr_Right->setModel(m_table_err_right);
    ui->tableimp->setModel(m_table_imp);
    ui->tableimpup->setModel(m_table_imp_up);
    ui->tableimpup2->setModel(m_table_imp_up2);
    ui->tableMoni->setModel(m_table_moni);
    ui->impMoni->setModel(m_table_moni_imp);
    ui->tableLeft_Coupling->setModel(m_table_left_coupling);
    ui->tableRight_Coupling->setModel(m_table_right_coupling);
    ui->table_imp_coupling->setModel(m_table_imp_coupling);
    ui->tableCCSB_Left->setModel(m_CSB_CLR_LeftModel);
    ui->tableCCSB_Right->setModel(m_CSB_CLR_RightModel);
    ui->tableCSBO_Left->setModel(m_SBO_CLR_LeftModel);
    ui->tableCSBO_Right->setModel(m_SBO_CLR_RightModel);
    ui->tableimpup3->setModel(m_CSB_CLR_imp);
    ui->tableimpup4->setModel(m_SBO_CLR_imp);
    ui->tableleftpos->setModel(m_table_left_pos);
    ui->tablerightpos->setModel(m_table_right_pos);
    ui->tableimppos->setModel(m_table_imp_pos);
    ui->tablespacepos->setModel(m_table_space_pos);




    // On transforme les cases des tableaux en SpinBox
    ui->tableCSB_Right->setItemDelegate(m_customSpinBox);
    ui->tableCSB_Left->setItemDelegate(m_customSpinBox);
    ui->tableSBO_Right->setItemDelegate(m_customSpinBox);
    ui->tableSBO_Left->setItemDelegate(m_customSpinBox);
    ui->tableErr_left->setItemDelegate(m_customSpinBox);
    ui->tableErr_Right->setItemDelegate(m_customSpinBox);
    ui->tableimp->setItemDelegate(m_customSpinBox);
    ui->tableimpup->setItemDelegate(m_customSpinBox);
    ui->tableimpup2->setItemDelegate(m_customSpinBox);
    ui->tableRight_Coupling->setItemDelegate(m_customSpinBox);
    ui->tableLeft_Coupling->setItemDelegate(m_customSpinBox);
    ui->table_imp_coupling->setItemDelegate(m_customSpinBox);
    ui->tableCCSB_Left->setItemDelegate(m_customSpinBox);
    ui->tableCCSB_Right->setItemDelegate(m_customSpinBox);
    ui->tableCSBO_Left->setItemDelegate(m_customSpinBox);
    ui->tableCSBO_Right->setItemDelegate(m_customSpinBox);
    ui->tableimpup3->setItemDelegate(m_customSpinBox);
    ui->tableimpup4->setItemDelegate(m_customSpinBox);
    ui->tableleftpos->setItemDelegate(m_customSpinBox);
    ui->tablerightpos->setItemDelegate(m_customSpinBox);
    ui->tableimppos->setItemDelegate(m_customSpinBox);
    ui->tablespacepos->setItemDelegate(m_customSpinBox);






   // connexion de la TreeComboBox, des boutons on/off/Reset
   connect(ui->antArrayCombo, &TreeCombo::currentItemChanged, this, &AntArrayDialog::setFileName);
   connect(ui->all_off, SIGNAL(clicked()), this, SLOT(on_btn_off_clicked()));
   connect(ui->all_on, SIGNAL(clicked()), this, SLOT(on_btn_on_clicked()));
   connect(ui->reset_ph, SIGNAL(clicked()), this, SLOT(on_reset_ph_clicked()));

   // connexion des boutons de conversion dB/Pourcentage
   connect(ui->db, SIGNAL(clicked()), this, SLOT(on_db_clicked()));
   connect(ui->db2, SIGNAL(clicked()), this, SLOT(on_db_clicked()));
   connect(ui->dbls, SIGNAL(clicked()), this, SLOT(on_db_clicked()));
   connect(ui->dblc, SIGNAL(clicked()), this, SLOT(on_db_clicked()));
   connect(ui->dbrs, SIGNAL(clicked()), this, SLOT(on_db_clicked()));
   connect(ui->dbrc, SIGNAL(clicked()), this, SLOT(on_db_clicked()));
   connect(ui->button_left_coupling,SIGNAL(clicked()),this,SLOT(on_db_clicked()));
   connect(ui->button_right_coupling,SIGNAL(clicked()),this,SLOT(on_db_clicked()));

   //Synchronisation des scrollbar
   connect(ui->tableCSB_Left->horizontalScrollBar(), SIGNAL(valueChanged(int)), ui->tableCSB_Right->horizontalScrollBar(), SLOT(setValue(int)));
   connect(ui->tableCSB_Right->horizontalScrollBar(), SIGNAL(valueChanged(int)), ui->tableCSB_Left->horizontalScrollBar(), SLOT(setValue(int)));
   connect(ui->tableSBO_Left->horizontalScrollBar(), SIGNAL(valueChanged(int)), ui->tableSBO_Right->horizontalScrollBar(), SLOT(setValue(int)));
   connect(ui->tableSBO_Right->horizontalScrollBar(), SIGNAL(valueChanged(int)), ui->tableSBO_Left->horizontalScrollBar(), SLOT(setValue(int)));
   connect(ui->tableErr_left->horizontalScrollBar(), SIGNAL(valueChanged(int)), ui->tableErr_Right->horizontalScrollBar(), SLOT(setValue(int)));
   connect(ui->tableErr_Right->horizontalScrollBar(), SIGNAL(valueChanged(int)), ui->tableErr_left->horizontalScrollBar(), SLOT(setValue(int)));
   connect(ui->tableLeft_Coupling->horizontalScrollBar(),SIGNAL(valueChanged(int)),ui->tableRight_Coupling->horizontalScrollBar(),SLOT(setValue(int)));
   connect(ui->tableRight_Coupling->horizontalScrollBar(),SIGNAL(valueChanged(int)),ui->tableLeft_Coupling->horizontalScrollBar(),SLOT(setValue(int)));
   ui->db->setCheckable(true);
   ui->db2->setCheckable(true);
   ui->dbls->setCheckable(true);
   ui->dbrs->setCheckable(true);
   ui->dblc->setCheckable(true);
   ui->dbrc->setCheckable(true);
   ui->button_left_coupling->setCheckable(true);
   ui->button_right_coupling->setCheckable(true);





}

//Ajout des fichiers dans le répertoire dont le nom est contenu dans info
//au model
void AntArrayDialog::addFiles(QStandardItem *root, QFileInfo dir)
{
    //On ajoute le nom du répertoire au niveau racine de
    //la ComboBox
    QStandardItem *item = new QStandardItem(dir.fileName());
    root->appendRow(item);

    //On recherche les fichiers contenus dans le répertoire
    QString path = dir.path() + "/" + dir.fileName();
    QDir l_dir = QDir(path);
    QFileInfoList list = l_dir.entryInfoList();

    //On ajoute les fichiers comme enfant de item
    int i = 0;
    foreach (QFileInfo info, list)
    {
        if(info.fileName()!="." && info.fileName()!="..")
        {
            item->setChild(i, 0, new QStandardItem(info.fileName()));
            i++;
        }
    }
}

// pas de single avec moins de 16 antennes , le plus grand single est 14
void AntArrayDialog::setFileName(QString text)
{
    QString path = m_antArrayDir + "/" + text;
    ui->arraySelect->setText(path);


    antArray.loadFile(path);



    qInfo() << "Number of antennas: " << antArray.getNbOfAnt();
    int nbOfAnt = antArray.getNbOfAnt();
    if (nbOfAnt % 2 == 0) {

        m_CSB_CRS_RightModel->clear();
        m_CSB_CRS_RightModel->insertRows(0, 2);
        m_CSB_CRS_RightModel->insertColumns(0, nbOfAnt/2);

        m_CSB_CRS_LeftModel->clear();
        m_CSB_CRS_LeftModel->insertRows(0, 2);
        m_CSB_CRS_LeftModel->insertColumns(0, nbOfAnt/2);

        m_SBO_CRS_RightModel->clear();
        m_SBO_CRS_RightModel->insertRows(0, 2);
        m_SBO_CRS_RightModel->insertColumns(0, nbOfAnt/2);

        m_SBO_CRS_LeftModel->clear();
        m_SBO_CRS_LeftModel->insertRows(0, 2);
        m_SBO_CRS_LeftModel->insertColumns(0, nbOfAnt/2);

        m_table_err_left->clear();
        m_table_err_left->insertRows(0, 2);
        m_table_err_left->insertColumns(0,nbOfAnt/2);

        m_table_err_right->clear();
        m_table_err_right->insertRows(0, 2);
        m_table_err_right->insertColumns(0,nbOfAnt/2);

       m_table_left_coupling->clear();
       m_table_left_coupling->insertRows(0, 2);
       m_table_left_coupling->insertColumns(0, nbOfAnt/2);

       m_table_right_coupling->clear();
       m_table_right_coupling->insertRows(0, 2);
       m_table_right_coupling->insertColumns(0, nbOfAnt/2);

       m_CSB_CLR_RightModel->clear();
       m_CSB_CLR_RightModel->insertRows(0,2);
       m_CSB_CLR_RightModel->insertColumns(0,nbOfAnt/2);

       m_CSB_CLR_LeftModel->clear();
       m_CSB_CLR_LeftModel->insertRows(0,2);
       m_CSB_CLR_LeftModel->insertColumns(0,nbOfAnt/2);

       m_SBO_CLR_LeftModel->clear();
       m_SBO_CLR_LeftModel->insertRows(0,2);
       m_SBO_CLR_LeftModel->insertColumns(0,nbOfAnt/2);

       m_SBO_CLR_RightModel->clear();
       m_SBO_CLR_RightModel->insertRows(0,2);
       m_SBO_CLR_RightModel->insertColumns(0,nbOfAnt/2);

       m_table_left_pos->clear();
       m_table_left_pos->insertRows(0,3);
       m_table_left_pos->insertColumns(0,nbOfAnt/2);

       m_table_right_pos->clear();
       m_table_right_pos->insertRows(0,3);
       m_table_right_pos->insertColumns(0,nbOfAnt/2);

       m_table_space_pos->clear();
       m_table_space_pos->insertRows(0,1);
       m_table_space_pos->insertColumns(0,nbOfAnt/2);



    }

    else {
        m_CSB_CRS_RightModel->clear();
        m_CSB_CRS_RightModel->insertRows(0, 2);
        m_CSB_CRS_RightModel->insertColumns(0, nbOfAnt/2);

        m_CSB_CRS_LeftModel->clear();
        m_CSB_CRS_LeftModel->insertRows(0, 2);
        m_CSB_CRS_LeftModel->insertColumns(0, nbOfAnt/2);

        m_SBO_CRS_RightModel->clear();
        m_SBO_CRS_RightModel->insertRows(0, 2);
        m_SBO_CRS_RightModel->insertColumns(0, nbOfAnt/2);

        m_SBO_CRS_LeftModel->clear();
        m_SBO_CRS_LeftModel->insertRows(0, 2);
        m_SBO_CRS_LeftModel->insertColumns(0, nbOfAnt/2);

        m_table_err_left->clear();
        m_table_err_left->insertRows(0, 2);
        m_table_err_left->insertColumns(0,nbOfAnt/2);

        m_table_err_right->clear();
        m_table_err_right->insertRows(0, 2);
        m_table_err_right->insertColumns(0,nbOfAnt/2);


        m_table_left_coupling->clear();
        m_table_left_coupling->insertRows(0, 2);
        m_table_left_coupling->insertColumns(0, nbOfAnt/2);

        m_table_right_coupling->clear();
        m_table_right_coupling->insertRows(0, 2);
        m_table_right_coupling->insertColumns(0, nbOfAnt/2);

        m_CSB_CLR_RightModel->clear();
        m_CSB_CLR_RightModel->insertRows(0,2);
        m_CSB_CLR_RightModel->insertColumns(0,nbOfAnt/2);

        m_CSB_CLR_LeftModel->clear();
        m_CSB_CLR_LeftModel->insertRows(0,2);
        m_CSB_CLR_LeftModel->insertColumns(0,nbOfAnt/2);

        m_SBO_CLR_RightModel->clear();
        m_SBO_CLR_RightModel->insertRows(0,2);
        m_SBO_CLR_RightModel->insertColumns(0,nbOfAnt/2);

        m_SBO_CLR_LeftModel->clear();
        m_SBO_CLR_LeftModel->insertRows(0,2);
        m_SBO_CLR_LeftModel->insertColumns(0,nbOfAnt/2);

        m_table_left_pos->clear();
        m_table_left_pos->insertRows(0,3);
        m_table_left_pos->insertColumns(0,nbOfAnt/2);

        m_table_right_pos->clear();
        m_table_right_pos->insertRows(0,3);
        m_table_right_pos->insertColumns(0,nbOfAnt/2);

        m_table_space_pos->clear();
        m_table_space_pos->insertRows(0,1);
        m_table_space_pos->insertColumns(0,nbOfAnt/2);


        // Création de la troisième QTableView si on a un nombre d'antennes impair
        m_table_imp->clear();
        m_table_imp->insertRows(0,2);
        m_table_imp->insertColumns(0,1);

        m_table_imp_up->clear();
        m_table_imp_up->insertRows(0,2);
        m_table_imp_up->insertColumns(0,1);

        m_table_imp_up2->clear();
        m_table_imp_up2->insertRows(0,2);
        m_table_imp_up2->insertColumns(0,1);

        m_table_imp_coupling->clear();
        m_table_imp_coupling->insertRows(0,2);
        m_table_imp_coupling->insertColumns(0,1);

        m_CSB_CLR_imp->clear();
        m_CSB_CLR_imp->insertRows(0,2);
        m_CSB_CLR_imp->insertColumns(0,1);

        m_SBO_CLR_imp->clear();
        m_SBO_CLR_imp->insertRows(0,2);
        m_SBO_CLR_imp->insertColumns(0,1);

        m_table_imp_pos->clear();
        m_table_imp_pos->insertRows(0,2);
        m_table_imp_pos->insertColumns(0,1);



    }



    //Titres des colonnes
    QStringList labelListH;
    for (int c=0; c<nbOfAnt/2; c++)
    {
        labelListH.append(QString("A%0").arg(c+1));
    }
    m_CSB_CRS_RightModel->setHorizontalHeaderLabels(labelListH);
    m_SBO_CRS_RightModel->setHorizontalHeaderLabels(labelListH);
    m_table_err_right->setHorizontalHeaderLabels(labelListH);
    m_table_right_coupling->setHorizontalHeaderLabels(labelListH);
    m_CSB_CLR_RightModel->setHorizontalHeaderLabels(labelListH);
    m_SBO_CLR_RightModel->setHorizontalHeaderLabels(labelListH);
    m_table_right_pos->setHorizontalHeaderLabels(labelListH);


    labelListH.clear();
    // On rajoute +1 pour que la dernière case du tableau se retrouve dans la troisième QTableView
    if (nbOfAnt%2 != 0) {
        for (int c=nbOfAnt; c>nbOfAnt/2+1; c--)
        {
            labelListH.append(QString("A%0").arg(c));
        }
    }

    else {
        for (int c=nbOfAnt; c>nbOfAnt/2; c--)
        {
            labelListH.append(QString("A%0").arg(c));
        }
    }


    m_CSB_CRS_LeftModel->setHorizontalHeaderLabels(labelListH);
    m_SBO_CRS_LeftModel->setHorizontalHeaderLabels(labelListH);
    m_table_left_coupling->setHorizontalHeaderLabels(labelListH);
    m_table_err_left->setHorizontalHeaderLabels(labelListH);
    m_table_left_coupling->setHorizontalHeaderLabels(labelListH);
    m_CSB_CLR_LeftModel->setHorizontalHeaderLabels(labelListH);
    m_SBO_CLR_LeftModel->setHorizontalHeaderLabels(labelListH);
    m_table_left_pos->setHorizontalHeaderLabels(labelListH);



    labelListH.clear();







    if (nbOfAnt%2 != 0) {
        labelListH.clear();
        int imp = (nbOfAnt+1)/2;
        labelListH.append(QString("A%0").arg(imp));
        m_table_imp->setHorizontalHeaderLabels(labelListH);
        m_table_imp_up->setHorizontalHeaderLabels(labelListH);
        m_table_imp_up2->setHorizontalHeaderLabels(labelListH);
        m_table_imp_coupling->setHorizontalHeaderLabels(labelListH);
        m_CSB_CLR_imp->setHorizontalHeaderLabels(labelListH);
        m_SBO_CLR_imp->setHorizontalHeaderLabels(labelListH);
        m_table_imp_pos->setHorizontalHeaderLabels(labelListH);
    }



    //Titres des lignes
    QStringList labelListV;
    labelListV.append("        Amp");
    labelListV.append("        Ph");
    m_CSB_CRS_RightModel->setVerticalHeaderLabels(labelListV);
    m_CSB_CRS_LeftModel->setVerticalHeaderLabels(labelListV);
    m_SBO_CRS_RightModel->setVerticalHeaderLabels(labelListV);
    m_SBO_CRS_LeftModel->setVerticalHeaderLabels(labelListV);
    m_table_err_left->setVerticalHeaderLabels(labelListV);
    m_table_err_right->setVerticalHeaderLabels(labelListV);
    m_table_imp->setVerticalHeaderLabels(labelListV);
    m_table_imp_up->setVerticalHeaderLabels(labelListV);
    m_table_imp_up2->setVerticalHeaderLabels(labelListV);
    m_table_left_coupling->setVerticalHeaderLabels(labelListV);
    m_table_right_coupling->setVerticalHeaderLabels(labelListV);
    m_table_imp_coupling->setVerticalHeaderLabels(labelListV);
    m_CSB_CLR_LeftModel->setVerticalHeaderLabels(labelListV);
    m_CSB_CLR_RightModel->setVerticalHeaderLabels(labelListV);
    m_SBO_CLR_LeftModel->setVerticalHeaderLabels(labelListV);
    m_SBO_CLR_RightModel->setVerticalHeaderLabels(labelListV);
    m_CSB_CLR_imp->setVerticalHeaderLabels(labelListV);
    m_SBO_CLR_imp->setVerticalHeaderLabels(labelListV);

    labelListV.clear();
    labelListV.append("Δ X");
    labelListV.append("Δ Y");
    labelListV.append("Δ Z");

    m_table_left_pos->setVerticalHeaderLabels(labelListV);
    m_table_right_pos->setVerticalHeaderLabels(labelListV);




    // le dernier élément de chaque tableau s'étirera maintenant avec la QTableView
    ui->tableCSB_Left->horizontalHeader()->setStretchLastSection(true);
    ui->tableCSB_Right->horizontalHeader()->setStretchLastSection(true);
    ui->tableCSB_Left->verticalHeader()->setStretchLastSection(true);
    ui->tableCSB_Right->verticalHeader()->setStretchLastSection(true);
    ui->tableSBO_Left->horizontalHeader()->setStretchLastSection(true);
    ui->tableSBO_Right->horizontalHeader()->setStretchLastSection(true);
    ui->tableSBO_Left->verticalHeader()->setStretchLastSection(true);
    ui->tableSBO_Right->verticalHeader()->setStretchLastSection(true);
    ui->tableimp->verticalHeader()->setStretchLastSection(true);
    ui->tableimp->horizontalHeader()->setStretchLastSection(true);
    ui->tableimpup->verticalHeader()->setStretchLastSection(true);
    ui->tableimpup->horizontalHeader()->setStretchLastSection(true);
    ui->tableimpup2->verticalHeader()->setStretchLastSection(true);
    ui->tableimpup2->horizontalHeader()->setStretchLastSection(true);
    ui->tableimpup3->verticalHeader()->setStretchLastSection(true);
    ui->tableimpup3->horizontalHeader()->setStretchLastSection(true);
    ui->tableimpup4->verticalHeader()->setStretchLastSection(true);
    ui->tableimpup4->horizontalHeader()->setStretchLastSection(true);
    ui->tableErr_Right->verticalHeader()->setStretchLastSection(true);
    ui->tableErr_Right->horizontalHeader()->setStretchLastSection(true);
    ui->tableErr_left->verticalHeader()->setStretchLastSection(true);
    ui->tableErr_left->horizontalHeader()->setStretchLastSection(true);
    ui->tableRight_Coupling->verticalHeader()->setStretchLastSection(true);
    ui->tableRight_Coupling->horizontalHeader()->setStretchLastSection(true);
    ui->tableLeft_Coupling->verticalHeader()->setStretchLastSection(true);
    ui->tableLeft_Coupling->horizontalHeader()->setStretchLastSection(true);
    ui->table_imp_coupling->verticalHeader()->setStretchLastSection(true);
    ui->table_imp_coupling->horizontalHeader()->setStretchLastSection(true);
    ui->tableCCSB_Left->verticalHeader()->setStretchLastSection(true);
    ui->tableCCSB_Left->horizontalHeader()->setStretchLastSection(true);
    ui->tableCCSB_Right->verticalHeader()->setStretchLastSection(true);
    ui->tableCCSB_Right->horizontalHeader()->setStretchLastSection(true);
    ui->tableCSBO_Left->verticalHeader()->setStretchLastSection(true);
    ui->tableCSBO_Right->verticalHeader()->setStretchLastSection(true);
    ui->tableCSBO_Right->horizontalHeader()->setStretchLastSection(true);
    ui->tableleftpos->verticalHeader()->setStretchLastSection(true);
    ui->tableleftpos->horizontalHeader()->setStretchLastSection(true);
    ui->tablerightpos->verticalHeader()->setStretchLastSection(true);
    ui->tablerightpos->horizontalHeader()->setStretchLastSection(true);





    //On récupère les données à afficher
    QVector<double> ampCSB_CRS = antArray.getAmpCSB_CRS();
    QVector<double> phCSB_CRS = antArray.getPhCSB_CRS();
    QVector<double> ampSBO_CRS = antArray.getAmpSBO_CRS();
    QVector<double> phSBO_CRS = antArray.getPhSBO_CRS();
    QVector<double> m_ampAntError = antArray.getAmpAntError();
    QVector<double> m_phAntError = antArray.getPhAntError();
    QVector<double> m_ampCoupling = antArray.getAmpCoupling();
    QVector<double> m_phCoupling = antArray.getPhCoupling();
    QVector<double> ampCSB_CLR = antArray.getAmpCSB_CLR();
    QVector<double> phCSB_CLR = antArray.getPhCSB_CLR();
    QVector<double> ampSBO_CLR = antArray.getAmpSBO_CLR();
    QVector<double> phSBO_CLR = antArray.getPhSBO_CLR();
    QVector<double> antPosX = antArray.getantPos_X();
    QVector<double> antPosY = antArray.getantPos_Y();
    QVector<double> antPosZ = antArray.getantPos_Z();









    //Les données sont copié dans le modèle
    QModelIndex index;
    for (int i=0; i<nbOfAnt/2; i++)
    {
        index = m_CSB_CRS_RightModel->index(0, i, QModelIndex());
        m_CSB_CRS_RightModel->setData(index, ampCSB_CRS.at(i));
        m_CSB_CRS_RightModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_CSB_CRS_RightModel->index(1, i, QModelIndex());
        m_CSB_CRS_RightModel->setData(index, phCSB_CRS.at(i));
        m_CSB_CRS_RightModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        ui->tableCSB_Right->setColumnWidth(i, 100);

        index = m_CSB_CLR_RightModel->index(0, i, QModelIndex());
        m_CSB_CLR_RightModel->setData(index, ampCSB_CLR.at(i));
        m_CSB_CLR_RightModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_CSB_CLR_RightModel->index(1, i, QModelIndex());
        m_CSB_CLR_RightModel->setData(index, phCSB_CLR.at(i));
        m_CSB_CLR_RightModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        ui->tableCCSB_Right->setColumnWidth(i, 100);

        index = m_SBO_CRS_RightModel->index(0, i, QModelIndex());
        m_SBO_CRS_RightModel->setData(index, ampSBO_CRS.at(i));
        m_SBO_CRS_RightModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_SBO_CRS_RightModel->index(1, i, QModelIndex());
        m_SBO_CRS_RightModel->setData(index, phSBO_CRS.at(i));
        m_SBO_CRS_RightModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        ui->tableSBO_Right->setColumnWidth(i, 100);

        index = m_SBO_CLR_RightModel->index(0, i, QModelIndex());
        m_SBO_CLR_RightModel->setData(index, ampSBO_CLR.at(i));
        m_SBO_CLR_RightModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_SBO_CLR_RightModel->index(1, i, QModelIndex());
        m_SBO_CLR_RightModel->setData(index, phSBO_CLR.at(i));
        m_SBO_CLR_RightModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        ui->tableCSBO_Right->setColumnWidth(i, 100);

        index = m_table_err_right->index(0, i, QModelIndex());
        m_table_err_right->setData(index,m_ampAntError.at(i));
        m_table_err_right->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_table_err_right->index(1, i, QModelIndex());
        m_table_err_right->setData(index,m_phAntError.at(i));
        m_table_err_right->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_table_right_coupling->index(0, i, QModelIndex());
        m_table_right_coupling->setData(index,m_ampCoupling.at(i));
        m_table_right_coupling->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);

        index = m_table_right_coupling->index(1, i, QModelIndex());
        m_table_right_coupling->setData(index,m_phCoupling.at(i));
        m_table_right_coupling->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);

        index = m_table_right_pos->index(0,i,QModelIndex());
        m_table_right_pos->setData(index,antPosX.at(i));
        m_table_right_pos->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);

        index = m_table_right_pos->index(1,i,QModelIndex());
        m_table_right_pos->setData(index,antPosY.at(i));
        m_table_right_pos->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);

        index = m_table_right_pos->index(2,i,QModelIndex());
        m_table_right_pos->setData(index,antPosZ.at(i));
        m_table_right_pos->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);


        // CLEARANCE : QUE POUR LES DUAL





    }

    for (int i=0; i<nbOfAnt/2; i++)
    {
        index = m_CSB_CRS_LeftModel->index(0, i, QModelIndex());
        m_CSB_CRS_LeftModel->setData(index, ampCSB_CRS.at(i));
        m_CSB_CRS_LeftModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_CSB_CRS_LeftModel->index(1, i, QModelIndex());
        m_CSB_CRS_LeftModel->setData(index, phCSB_CRS.at(i));
        m_CSB_CRS_LeftModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        ui->tableCSB_Left->setColumnWidth(i, 100);

        index = m_CSB_CLR_LeftModel->index(0, i, QModelIndex());
        m_CSB_CLR_LeftModel->setData(index, ampCSB_CLR.at(i));
        m_CSB_CLR_LeftModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_CSB_CLR_LeftModel->index(1, i, QModelIndex());
        m_CSB_CLR_LeftModel->setData(index, phCSB_CLR.at(i));
        m_CSB_CLR_LeftModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);


        ui->tableCCSB_Left->setColumnWidth(i, 100);



        index = m_SBO_CRS_LeftModel->index(0, i, QModelIndex());
        m_SBO_CRS_LeftModel->setData(index, ampSBO_CRS.at(i));
        m_SBO_CRS_LeftModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_SBO_CRS_LeftModel->index(1, i, QModelIndex());
        m_SBO_CRS_LeftModel->setData(index, phSBO_CRS.at(i));
        m_SBO_CRS_LeftModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);



        ui->tableSBO_Left->setColumnWidth(i, 100);

        index = m_SBO_CLR_LeftModel->index(0, i, QModelIndex());
        m_SBO_CLR_LeftModel->setData(index, ampSBO_CLR.at(i));
        m_SBO_CLR_LeftModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_SBO_CLR_LeftModel->index(1, i, QModelIndex());
        m_SBO_CLR_LeftModel->setData(index, phSBO_CLR.at(i));
        m_SBO_CLR_LeftModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        ui->tableCSBO_Left->setColumnWidth(i, 100);


        index = m_table_err_left->index(0, i, QModelIndex());
        m_table_err_left->setData(index,m_ampAntError.at(i));
        m_table_err_left->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_table_err_left->index(1, i, QModelIndex());
        m_table_err_left->setData(index,m_phAntError.at(i));
        m_table_err_left->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_table_left_coupling->index(0, i, QModelIndex());
        m_table_left_coupling->setData(index,m_ampCoupling.at(i));
        m_table_left_coupling->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_table_left_coupling->index(1, i, QModelIndex());
        m_table_left_coupling->setData(index,m_phCoupling.at(i));
        m_table_left_coupling->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_table_left_pos->index(0,i,QModelIndex());
        m_table_left_pos->setData(index,antPosX.at(i));
        m_table_left_pos->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);

        index = m_table_left_pos->index(1,i,QModelIndex());
        m_table_left_pos->setData(index,antPosY.at(i));
        m_table_left_pos->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);

        index = m_table_left_pos->index(2,i,QModelIndex());
        m_table_left_pos->setData(index,antPosZ.at(i));
        m_table_left_pos->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);



        // Si on a un nombre d'antennes impair, on cache la troisième QTableView de chaque section (on met leur taille maximum à 0)
        ui->tableimp->setMaximumWidth(0);
        ui->tableimp->setMaximumHeight(0);
        ui->tableimpup->setMaximumWidth(0);
        ui->tableimpup->setMaximumHeight(0);
        ui->tableimpup2->setMaximumWidth(0);
        ui->tableimpup2->setMaximumHeight(0);
        ui->table_imp_coupling->setMaximumHeight(0);
        ui->table_imp_coupling->setMaximumWidth(0);
        ui->tableimpup3->setMaximumHeight(0);
        ui->tableimpup3->setMaximumWidth(0);
        ui->tableimpup4->setMaximumWidth(0);
        ui->tableimpup4->setMaximumHeight(0);
        ui->tableimppos->setMaximumHeight(0);

        ui->tableimp->setColumnWidth(i, 100);
        ui->tableimp->setColumnWidth(i, 100);
        ui->tableimpup->setColumnWidth(i, 100);
        ui->tableimpup->setColumnWidth(i, 100);
        ui->tableimpup2->setColumnWidth(i, 100);
        ui->tableimpup2->setColumnWidth(i, 100);
        ui->table_imp_coupling->setColumnWidth(i, 100);

    }

    if (nbOfAnt%2 != 0) {
        int imp = (nbOfAnt+1)/2;
        index = m_table_imp->index(0,0,QModelIndex());
        m_table_imp->setData(index,m_ampAntError.at(imp));
        m_table_imp->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_table_imp->index(1,0,QModelIndex());
        m_table_imp->setData(index,m_phAntError.at(imp));
        m_table_imp->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);


        index = m_table_imp_up->index(0,0,QModelIndex());
        m_table_imp_up->setData(index,ampCSB_CRS.at(imp));
        m_table_imp_up->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_table_imp_up->index(1,0,QModelIndex());
        m_table_imp_up->setData(index,phCSB_CRS.at(imp));
        m_table_imp_up->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_table_imp_up2->index(0,0,QModelIndex());
        m_table_imp_up2->setData(index,ampSBO_CRS.at(imp));
        m_table_imp_up2->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_table_imp_up2->index(1,0,QModelIndex());
        m_table_imp_up2->setData(index,phSBO_CRS.at(imp));
        m_table_imp_up2->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);


        index = m_table_imp_coupling->index(0,0,QModelIndex());
        m_table_imp_coupling->setData(index,m_ampCoupling.at(imp));
        m_table_imp_coupling->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_table_imp_coupling->index(1,0,QModelIndex());
        m_table_imp_coupling->setData(index,m_phCoupling.at(imp));
        m_table_imp_coupling->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_CSB_CLR_imp->index(0,0,QModelIndex());
        m_CSB_CLR_imp->setData(index,ampCSB_CLR.at(imp));
        m_CSB_CLR_imp->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_CSB_CLR_imp->index(1,0,QModelIndex());
        m_CSB_CLR_imp->setData(index,phCSB_CLR.at(imp));
        m_CSB_CLR_imp->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_SBO_CLR_imp->index(0,0,QModelIndex());
        m_SBO_CLR_imp->setData(index,ampSBO_CLR.at(imp));
        m_SBO_CLR_imp->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

        index = m_SBO_CLR_imp->index(1,0,QModelIndex());
        m_SBO_CLR_imp->setData(index,phSBO_CLR.at(imp));
        m_SBO_CLR_imp->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);








        // On réaffiche la troisième QTableView
        ui->tableimp->setMaximumWidth(16777215);
        ui->tableimp->setMaximumHeight(16777215);
        ui->tableimpup->setMaximumWidth(16777215);
        ui->tableimpup->setMaximumHeight(16777215);
        ui->tableimpup2->setMaximumWidth(16777215);
        ui->tableimpup2->setMaximumHeight(16777215);
        ui->table_imp_coupling->setMaximumHeight(16777215);
        ui->table_imp_coupling->setMaximumWidth(16777215);
        ui->tableimpup3->setMaximumHeight(16777215);
        ui->tableimpup3->setMaximumWidth(16777215);
        ui->tableimpup4->setMaximumWidth(16777215);
        ui->tableimpup4->setMaximumHeight(16777215);
        ui->tableimppos->setMaximumHeight(16777215);
    }

    if (antArray.getArrayType() == 0) {
        ui->tabWidget->removeTab(3);
        ui->tabWidget->removeTab(2);


    }

    else if (antArray.getArrayType() == 1) {
       QWidget *tab6 =ui->tabWidget->findChild<QWidget*>("tab_6");
       qApp->processEvents();
       QString *tab6str = new QString("CSB CLR");

       ui->tabWidget->addTab(tab6,*tab6str);

       QString *tab7str = new QString("SBO CLR");
      QWidget *tab7 = ui->tabWidget->findChild<QWidget*>("tab_7");
       ui->tabWidget->addTab(tab7,*tab7str);

    }
    // On affiche la photo selon l'antenne sé
    QString path2 = QDir::currentPath();
    int index2 = path2.lastIndexOf("/");
    m_antArrayDir2 = path2.mid(0,index2);
    if (path.contains("NM_7220A")) {
        m_antArrayDir2.append("/Photos/NORMARC/DSC01005.JPG");
    }

    else if (path.contains("NM_7232A")) {
        m_antArrayDir2.append("/Photos/NORMARC/Zurich 32 ant pour ATOLL.png");

    }

    else if(path.contains("SEL12_3")) {
        m_antArrayDir2.append("/Photos/SEL/LLZ-10.jpg");

    }






    QPixmap pm(m_antArrayDir2); // <- path to image file
    ui->label_22->setPixmap(pm);
    ui->label_22->setScaledContents(true);

    //On récupère la taille de la fenêtre
    QRect rect = QDialog::frameGeometry();
    //QDialog::setFixedWidth(600);
    rect = ui->tabWidget->rect();

    qDebug() << "Width 1: " << rect.width();
    //Calcul de la largeur du tableau
    m_colWidth = ui->tableCSB_Right->columnWidth(1);
    qDebug() << "la column width de la csb right : " << m_colWidth;
    m_vHeaderWidth = ui->tableCSB_Right->verticalHeader()->width();
    m_tableWidth = m_vHeaderWidth + antArray.getNbOfAnt()*m_colWidth - 50;
    if (m_tableWidth+20 < rect.width())
        qDebug() << "Test 1";//ui->tableView->setFixedWidth(m_tableWidth);
    else
    {
        qDebug() << "Test 2";



    }

    rect = QDialog::frameGeometry();


    //Calcul de la hauteur des tableaux
    int hHeaderHeight = ui->tableCSB_Right->horizontalHeader()->height();
    int rowHeight = ui->tableCSB_Right->rowHeight(0);
    rect = ui->tableCSB_Right->horizontalScrollBar()->rect();
    int tableHeight = 2*rowHeight + hHeaderHeight + rect.height()+2;
    ui->tableCSB_Right->setFixedHeight(tableHeight);
    ui->tableCSB_Left->setFixedHeight(tableHeight);
    ui->tableSBO_Right->setFixedHeight(tableHeight);
    ui->tableSBO_Left->setFixedHeight(tableHeight);
    ui->tableCCSB_Right->setFixedHeight(tableHeight);
    ui->tableCCSB_Left->setFixedHeight(tableHeight);
    ui->tableCSBO_Right->setFixedHeight(tableHeight);
    ui->tableCSBO_Left->setFixedHeight(tableHeight);
    ui->tableErr_Right->setFixedHeight(tableHeight);
    ui->tableErr_left->setFixedHeight(tableHeight);
    ui->tableimp->setFixedHeight(tableHeight-20);
    ui->tableimpup->setFixedHeight(tableHeight-20);
    ui->tableimpup2->setFixedHeight(tableHeight-20);
    ui->tableimpup3->setFixedHeight(tableHeight-20);
    ui->tableimpup4->setFixedHeight(tableHeight-20);
    ui->tableLeft_Coupling->setFixedHeight(tableHeight);
    ui->tableRight_Coupling->setFixedHeight(tableHeight);
    ui->table_imp_coupling->setFixedHeight(tableHeight-20);

    // on soustrait -20 au troisièmes QTableView de chaque section pour que chaque TableView aient la même taille


}



void AntArrayDialog::on_btn_off_clicked()
{
    QModelIndex index;
    QVector<double> m_phAntError = antArray.getPhAntError();
    int nbOfAnt = antArray.getNbOfAnt();
    for (int i=0; i<nbOfAnt/2; i++)
    {
        index = m_table_err_left->index(0, i, QModelIndex());
        m_table_err_left->setData(index,m_phAntError.at(i));
        m_table_err_left->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

    }

    for (int i=0; i<nbOfAnt/2; i++)
    {
        index = m_table_err_right->index(0, i, QModelIndex());
        m_table_err_right->setData(index,m_phAntError.at(i));
        m_table_err_right->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);



    }

    if (nbOfAnt%2 != 0) {
        index = m_table_imp->index(0,0,QModelIndex());
        m_table_imp->setData(index,m_phAntError.at(0));
        m_table_imp->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);
    }


}


void AntArrayDialog::on_btn_on_clicked()
{
    QModelIndex index;
    QVector<double> m_ampAntError = antArray.getAmpAntError();
    int nbOfAnt = antArray.getNbOfAnt();
    for (int i=0; i<nbOfAnt/2; i++)
    {
        index = m_table_err_left->index(0, i, QModelIndex());
        m_table_err_left->setData(index,m_ampAntError.at(i));
        m_table_err_left->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

    }



    for (int i=0; i<nbOfAnt/2; i++)
    {
        index = m_table_err_right->index(0, i, QModelIndex());
        m_table_err_right->setData(index,m_ampAntError.at(i));
        m_table_err_right->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

    }

    if (nbOfAnt%2 != 0) {
        index = m_table_imp->index(0,0,QModelIndex());
        m_table_imp->setData(index,m_ampAntError.at(0));
        m_table_imp->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);
    }
}


void AntArrayDialog::on_reset_ph_clicked()
{
    QModelIndex index;
    QVector<double> m_phAntError = antArray.getPhAntError();
    int nbOfAnt = antArray.getNbOfAnt();
    for (int i=0; i<nbOfAnt/2; i++)
    {
        index = m_table_err_left->index(1, i, QModelIndex());
        m_table_err_left->setData(index,m_phAntError.at(i));
        m_table_err_left->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

    }

    for (int i=0; i<nbOfAnt/2; i++)
    {
        index = m_table_err_right->index(1, i, QModelIndex());
        m_table_err_right->setData(index,m_phAntError.at(i));
        m_table_err_right->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

    }

    if (nbOfAnt%2 != 0) {
        index = m_table_imp->index(1,0,QModelIndex());
        m_table_imp->setData(index,m_phAntError.at(0));
        m_table_imp->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);
    }
}


void AntArrayDialog::on_db_clicked()
{
        QModelIndex index;

        QVector<double> ampCSB_CRS = antArray.getAmpCSB_CRS();
        QVector<double> ampSBO_CRS = antArray.getAmpSBO_CRS();
        QVector<double> m_ampAntError = antArray.getAmpAntError();
        QVector<double> m_ampCoupling = antArray.getAmpCoupling();
        int nbOfAnt = antArray.getNbOfAnt();


            if (ui->db->isChecked() | ui->db2->isChecked() | ui->dblc->isChecked() | ui->dbls->isChecked() | ui->dbrc->isChecked() | ui->dbrs->isChecked() | ui->button_left_coupling->isChecked() | ui->button_right_coupling->isChecked()) {
                for (int i=0; i<nbOfAnt/2; i++)
                {
                    int conv = 20*log(m_ampAntError.at(i));
                    int convSBO = 20*log(ampSBO_CRS.at(i));
                    int convCSB = 20*log(ampCSB_CRS.at(i));
                    int convCoup = 20*log(m_ampCoupling.at(i));
                    index = m_table_err_left->index(0, i, QModelIndex());
                    m_table_err_left->setData(index,conv);
                    m_table_err_left->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

                    index = m_CSB_CRS_LeftModel->index(0,i,QModelIndex());
                    m_CSB_CRS_LeftModel->setData(index,convCSB);
                    m_CSB_CRS_LeftModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

                    index = m_SBO_CRS_LeftModel->index(0,i,QModelIndex());
                    m_SBO_CRS_LeftModel->setData(index,convSBO);
                    m_SBO_CRS_LeftModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

                    index = m_table_left_coupling->index(0,i,QModelIndex());
                    m_table_left_coupling->setData(index,convCoup);
                    m_table_left_coupling->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);
                    qDebug() << convCoup;


                }

                for (int i=0; i<nbOfAnt/2; i++)
                {
                    int conv = 20*log(m_ampAntError.at(i));
                    int convSBO = 20*log(ampSBO_CRS.at(i));
                    int convCSB = 20*log(ampCSB_CRS.at(i));
                    int convCoup = 20*log(m_ampCoupling.at(i));
                    index = m_table_err_right->index(0, i, QModelIndex());
                    m_table_err_right->setData(index,conv);
                    m_table_err_right->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

                    index = m_CSB_CRS_RightModel->index(0,i,QModelIndex());
                    m_CSB_CRS_RightModel->setData(index,convCSB);
                    m_CSB_CRS_RightModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

                    index = m_SBO_CRS_RightModel->index(0,i,QModelIndex());
                    m_SBO_CRS_RightModel->setData(index,convSBO);
                    m_SBO_CRS_RightModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

                    index = m_table_right_coupling->index(0,i,QModelIndex());
                    m_table_right_coupling->setData(index,convCoup);
                    m_table_right_coupling->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

                }



                                int conv = 20*log(m_ampAntError.at((nbOfAnt+1)/2));
                                int convSBO = 20*log(ampSBO_CRS.at((nbOfAnt+1)/2));
                                int convCSB = 20*log(ampCSB_CRS.at((nbOfAnt+1)/2));
                                int convCoup = 20*log(m_ampCoupling.at((nbOfAnt+1)/2));
                                qDebug() << convSBO;
                                index = m_table_imp->index(0,0,QModelIndex());
                                m_table_imp->setData(index,conv);
                                m_table_imp->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);

                                index = m_table_imp_up->index(0,0,QModelIndex());
                                m_table_imp_up->setData(index,convCSB);
                                m_table_imp_up->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);

                                index = m_table_imp_up2->index(0,0,QModelIndex());
                                m_table_imp_up2->setData(index,convSBO);
                                m_table_imp_up2->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);

                                index = m_table_imp_coupling->index(0,0,QModelIndex());
                                m_table_imp_coupling->setData(index,convCoup);
                                m_table_imp_coupling->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);



                ui->db->QPushButton::setText("dB");
                ui->db2->QPushButton::setText("dB");
                ui->dblc->QPushButton::setText("dB");
                ui->dbls->QPushButton::setText("dB");
                ui->dbrc->QPushButton::setText("dB");
                ui->dbrs->QPushButton::setText("dB");
                ui->button_left_coupling->QPushButton::setText("dB");
                ui->button_right_coupling->QPushButton::setText("dB");

                ui->db->repaint();
                qApp->processEvents();
            }

            else {
                for (int i=0; i<nbOfAnt/2; i++)
                {


                    index = m_table_err_left->index(0, i, QModelIndex());
                    m_table_err_left->setData(index,m_ampAntError.at(i));
                    m_table_err_left->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

                    index = m_CSB_CRS_LeftModel->index(0, i, QModelIndex());
                    m_CSB_CRS_LeftModel->setData(index, ampCSB_CRS.at(nbOfAnt-i-1));
                    m_CSB_CRS_LeftModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);



                    index = m_SBO_CRS_LeftModel->index(0, i, QModelIndex());
                    m_SBO_CRS_LeftModel->setData(index, ampSBO_CRS.at(nbOfAnt-i-1));
                    m_SBO_CRS_LeftModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

                    index = m_table_left_coupling->index(0,i,QModelIndex());
                    m_table_left_coupling->setData(index, m_ampCoupling.at(i));
                    m_table_left_coupling->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);



                }

                for (int i=0; i<nbOfAnt/2; i++)
                {


                    index = m_table_err_right->index(0, i, QModelIndex());
                    m_table_err_right->setData(index,m_ampAntError.at(i));
                    m_table_err_right->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

                    index = m_CSB_CRS_RightModel->index(0, i, QModelIndex());
                    m_CSB_CRS_RightModel->setData(index, ampCSB_CRS.at(i));
                    m_CSB_CRS_RightModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);



                    index = m_SBO_CRS_RightModel->index(0, i, QModelIndex());
                    m_SBO_CRS_RightModel->setData(index, ampSBO_CRS.at(i));
                    m_SBO_CRS_RightModel->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);

                    index = m_table_right_coupling->index(0,i,QModelIndex());
                    m_table_right_coupling->setData(index, m_ampCoupling.at(i));
                    m_table_right_coupling->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);





                }

                index = m_table_imp->index(0,0,QModelIndex());
                m_table_imp->setData(index,m_ampAntError.at((nbOfAnt+1)/2));
                m_table_imp->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);

                index = m_table_imp_up->index(0,0,QModelIndex());
                m_table_imp_up->setData(index,ampCSB_CRS.at((nbOfAnt+1)/2));
                m_table_imp_up->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);

                index = m_table_imp_up2->index(0,0,QModelIndex());
                m_table_imp_up2->setData(index,ampSBO_CRS.at((nbOfAnt+1)/2));
                m_table_imp_up2->setData(index,Qt::AlignCenter,Qt::TextAlignmentRole);


                index = m_table_imp_coupling->index(0,0,QModelIndex());
                m_table_imp_coupling->setData(index,m_ampCoupling.at((nbOfAnt+1)/2));
                m_table_imp_coupling->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);


                ui->db->QPushButton::setText("%");
                ui->db2->QPushButton::setText("%");
                ui->dbls->QPushButton::setText("%");
                ui->dbrs->QPushButton::setText("%");
                ui->dblc->QPushButton::setText("%");
                ui->dbrc->QPushButton::setText("%");
                ui->button_left_coupling->QPushButton::setText("%");
                ui->button_right_coupling->QPushButton::setText("%");
                ui->db->repaint();
                qApp->processEvents();
            }




        }




