#include "tree_combo.h"

TreeCombo::TreeCombo(QWidget *parent) : QComboBox(parent)
{

    //qInfo() << "test constructor";

    //Une ComboBox a par défaut une ListView pour afficher ses membres
    //On lui affecte ci-dessous une Treeview
    this->setView(m_tree);

    //On ne veut pas de titre en haut de la TreeView
    m_tree->setHeaderHidden(true);

    //Par défaut avec un ComboBox un click sur un des membres ferme la liste
    //Or on ne veut pa fermer la liste lorsqu'on clique sur un répertoire
    //Il faut donc réimplémenté certaines méthodes de la ComboBox
    m_tree->viewport()->installEventFilter(this);


}

bool TreeCombo::eventFilter(QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::MouseButtonPress && watched == m_tree->viewport())
    {
        //qInfo() << "Mouse pressed";
        QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
        QModelIndex index = view()->indexAt(mouseEvent->pos());

        //qInfo() << index.parent().row();
        //qInfo() << index.row();
        if (index.parent().row()==-1)
            m_hide = false;
        else
        {
            m_hide = true;
            m_currentFile = index.parent().data().toString() + "/" + index.data().toString();
            //qInfo() << m_currentFile;

        }

        qInfo() << "event filter";

    }
    return false;
}

void TreeCombo::showPopup()
{
    QComboBox::showPopup();
}

void TreeCombo::hidePopup()
{
    qInfo() << "hide popup = " << m_hide;
    if (m_hide==true)
    {
        QComboBox::hidePopup();
        emit currentItemChanged(m_currentFile);

    }


}
