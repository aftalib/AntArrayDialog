#ifndef SPINBOXDELAGATE_H
#define SPINBOXDELAGATE_H

#include <QObject>
#include <QStyleOptionViewItem>
#include <QStyledItemDelegate>
#include <QDoubleSpinBox>


class SpinBoxDelagate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit SpinBoxDelagate(QObject *parent = nullptr);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;
    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const override;

    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option,
                              const QModelIndex &index) const override;

signals:

};

#endif // SPINBOXDELAGATE_H
