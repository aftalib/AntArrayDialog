#include "antennaarray.h"

//Constructeur
AntennaArray::AntennaArray(QObject *parent, QString path) : QObject(parent) //Liste d'initialisation
{

    loadFile(path);
}

bool AntennaArray::loadFile(QString path)
{
    //Création de l'objet file de type QFile
    QFile file(path);

    //Test si le fichier exist
    if (!file.exists())
    {
        qWarning() << "File not found";
        return false;
    }

    //Test si le fichier a pu être ouvert
    if (!file.open(QIODevice::ReadOnly))
    {
        qWarning() << file.errorString();
        return false;
    }

    QString text = file.readAll();
    file.close();

    //Array name
    int indexEnd = path.indexOf(".ary");
    //int indexBegin = path.indexOf(QDir::separator(), -indexEnd);
    //int indexBegin = path.indexOf("/", -indexEnd);
    int indexBegin = path.lastIndexOf("/");

    m_arrayName = path.mid(indexBegin+1, indexEnd-indexBegin-1).trimmed();

    //Nb of antennas
    int indexLineLabel = text.indexOf("Nb of antennas");
    // retourne la position des deux points a partir de indexlinelabel
    indexBegin = text.indexOf(":", indexLineLabel) + 1;
    indexEnd = text.indexOf("\n", indexBegin);
    QString text2 = text.mid(indexBegin, indexEnd-indexBegin).trimmed();
    m_nbOfAnt = text2.toUInt();

    //Array type
    indexLineLabel = text.indexOf("Array type", indexEnd);
    indexBegin = text.indexOf(":", indexLineLabel) + 1;
    indexEnd = text.indexOf("(", indexBegin);
    text2 = text.mid(indexBegin, indexEnd-indexBegin).trimmed();
    m_arrayType = text2.toInt();

    //Heights
    indexLineLabel = text.indexOf("Heights");
    indexBegin = text.indexOf("=", indexLineLabel) + 1;
    indexEnd = text.indexOf(";", indexBegin);

    text2 = text.mid(indexBegin, indexEnd-indexBegin).trimmed();
    m_antHeights[0] = text2.toDouble();

    indexBegin = text.indexOf("=", indexEnd) + 1;
    indexEnd = text.indexOf("\n", indexBegin);
    m_antHeights[1] = text2.toDouble();

    //Ant Type
    indexLineLabel = text.indexOf("Ant Type");
    indexBegin = text.indexOf("=", indexLineLabel) + 1;
    indexEnd = text.indexOf("(", indexBegin);
    text2 = text.mid(indexBegin, indexEnd-indexBegin).trimmed();
    m_antType[0] = text2.toInt();

    indexBegin = text.indexOf("=", indexEnd) + 1;
    indexEnd = text.indexOf("(", indexBegin);
    text2 = text.mid(indexBegin, indexEnd-indexBegin).trimmed();
    m_antType[1] = text2.toInt();

    //Spacing type
    indexLineLabel = text.indexOf("Spacing Type");
    indexBegin = text.indexOf(":", indexLineLabel) + 1;
    indexEnd = text.indexOf("\n", indexBegin);
    text2 = text.mid(indexBegin, indexEnd-indexBegin).trimmed();
    m_spacingType = text2.toInt();

    //Base spacing
    indexLineLabel = text.indexOf("Base spacing");
    indexBegin = text.indexOf(":", indexLineLabel) + 1;
    indexEnd = text.indexOf("lambda", indexBegin);
    text2 = text.mid(indexBegin, indexEnd-indexBegin).trimmed();
    m_baseSpacing = text2.toDouble();

    //Array design frequency
    indexLineLabel = text.indexOf("Array design frequency", indexEnd);
    indexBegin = text.indexOf(":", indexLineLabel) + 1;
    indexEnd = text.indexOf("MHz", indexBegin);
    text2 = text.mid(indexBegin, indexEnd-indexBegin).trimmed();
    m_designFreq = text2.toDouble();

    //Spacing unit
    indexLineLabel = text.indexOf("Spacing unit", indexEnd);
    indexBegin = text.indexOf(":", indexLineLabel) + 1;
    indexEnd = text.indexOf("\n", indexBegin);
    text2 = text.mid(indexBegin, indexEnd-indexBegin).trimmed();
    m_spacingUnit = text2.toInt();

    readArrayData(text, "H buttons state", indexEnd, &m_heightBtState);
    readArrayData(text, "Antenna spacing", indexEnd, &m_spacing);
    readArrayData(text, "Ant pos X", indexEnd, &m_antPosErrorX);
    readArrayData(text, "Ant pos Y", indexEnd, &m_antPosErrorY);
    readArrayData(text, "Ant pos Z", indexEnd, &m_antPosErrorZ);

    readSingleData(text, "Array rotation", "\n", indexEnd, m_arrayRot);
    readSingleData(text, "Array Y offset", "\n", indexEnd, m_arrayY_Offset);

    readArrayData(text, "CSB CRS Amp", indexEnd, &m_ampCSB_CRS);
    readArrayData(text, "CSB CRS Phase", indexEnd, &m_phCSB_CRS);
    readArrayData(text, "SBO CRS Amp", indexEnd, &m_ampSBO_CRS);
    readArrayData(text, "SBO CRS Phase", indexEnd, &m_phSBO_CRS);
    readArrayData(text, "CSB CLEAR Amp", indexEnd, &m_ampCSB_CLR);
    readArrayData(text, "CSB CLEAR Phase", indexEnd, &m_phCSB_CLR);
    readArrayData(text, "SBO CLEAR Amp", indexEnd, &m_ampSBO_CLR);
    readArrayData(text, "SBO CLEAR Phase", indexEnd, &m_phSBO_CLR);
    readArrayData(text, "CSB CLEAR 2 Amp", indexEnd, &m_ampCSB_CLR2);
    readArrayData(text, "CSB CLEAR 2 Phase", indexEnd, &m_phCSB_CLR2);
    readArrayData(text, "SBO CLEAR 2 Amp", indexEnd, &m_ampSBO_CLR2);
    readArrayData(text, "SBO CLEAR 2 Phase", indexEnd, &m_phSBO_CLR2);

    readSingleData(text, "CLR2_On", "\n", indexEnd, m_CLR2_On);

    readArrayData(text, "Ant Error Amp", indexEnd, &m_ampAntError);
    readArrayData(text, "Ant Error Phase", indexEnd, &m_phAntError);
    readArrayData(text, "Coupling Amp", indexEnd, &m_ampCoupling);
    readArrayData(text, "Coupling Phase", indexEnd, &m_phCoupling);

    readSingleData(text, "Default Signal Generator", "(", indexEnd, m_defaultSigGen);
    readSingleData(text, "Default Course power", "W", indexEnd, m_defaultCRS_Power);
    readSingleData(text, "Default Clear power", "W", indexEnd, m_defaultCLR_Power);
    readSingleData(text, "Clear/Course ratio on CL", "d", indexEnd, m_CLR_CRS_Ratio);
    //readSingleData(text, "Azimut Clearance", "\n", indexEnd, m_CLR_Az);

    //Pour Azimut Clearance je ne peux pas utiliser la fonction readSingleData
    //car le caractère "°" n'est pas reconnu
    indexLineLabel = text.indexOf("Azimut Clearance", indexEnd);
    if (indexLineLabel==-1)
        qWarning() << "Azimut Clearance not found after index " << indexEnd;
    else
    {
        int indexBegin = text.indexOf(":", indexLineLabel) + 1;
        indexEnd = text.indexOf("\n", indexBegin);
        QString text2 = text.mid(indexBegin, indexEnd-indexBegin-2).trimmed();
        //qDebug() << text2;
        m_CLR_Az = text2.toDouble();
    }

    readSingleData(text, "DDM Clearance at Az", "\n", indexEnd, m_CLR_DDMatAz);
    readSingleData(text, "Index", "\n", indexEnd, m_photoIndex);
/*
    qInfo() << "Array name: " << m_arrayName;
    qInfo() << m_nbOfAnt;
    qInfo() << m_arrayType;
    qInfo() << m_antHeights[0];
    qInfo() << m_antHeights[1];
    qInfo() << m_antType[0];
    qInfo() << m_antType[1];
    qInfo() << m_spacingType;
    qInfo() << m_baseSpacing;
    qInfo() << m_designFreq;
    qInfo() << m_antPosErrorX;
    qInfo() << m_antPosErrorY;
    qInfo() << m_antPosErrorZ;
    qInfo() << m_arrayRot;
    qInfo() << m_arrayY_Offset;

    qInfo() << m_ampCSB_CRS;
    qInfo() << m_phCSB_CRS;
    qInfo() << m_ampSBO_CRS;
    qInfo() << m_phSBO_CRS;
    qInfo() << m_ampCSB_CLR;
    qInfo() << m_phCSB_CLR;
    qInfo() << m_ampSBO_CLR;
    qInfo() << m_phSBO_CLR;
    qInfo() << m_ampCSB_CLR2;
    qInfo() << m_phCSB_CLR2;
    qInfo() << m_ampSBO_CLR2;
    qInfo() << m_phSBO_CLR2;
    qInfo() << m_CLR2_On;

    qInfo() << m_ampAntError;
    qInfo() << m_phAntError;
    qInfo() << m_ampCoupling;
    qInfo() << m_phCoupling;
    qInfo() << m_defaultSigGen;
    qInfo() << m_defaultCRS_Power;
    qInfo() << m_defaultCLR_Power;
    qInfo() << m_CLR_CRS_Ratio;
    qInfo() << m_CLR_Az;
    qInfo() << m_CLR_DDMatAz;
    qInfo() << m_photoIndex;
*/

    //qInfo() << m_ampAntError<<;
   // qInfo() << m_phAntError <<;
    return true;
}

bool AntennaArray::saveFile(QString path)
{
    //Création de l'objet file de type QFile
    QFile file(path);

    //Test si le fichier a pu être ouvert
    if (!file.open(QIODevice::WriteOnly))
    {
        qWarning() << file.errorString();
        return false;
    }



    QByteArray line = m_arrayName.toLatin1();
    line.append("\n\n");

    //Méthode en passant par QString pour transformer le nombre
    //en QByteArray
    QString num = "Nb of antennas : " + QString::number(m_nbOfAnt);
    line.append(num.toLatin1());
    file.write(line);

    //Méthode en transformant directement le nombre en QByteArray
    line = "\n\nGeneral meccanic settings : \nHeights : H1 = ";
    QByteArray ba;
    line.append(ba.setNum(m_antHeights[0]));
    line.append("; H2 = ");
    line.append(ba.setNum(m_antHeights[1]));
    file.write(line);

    line = "\nAnt Type : H1 = ";
    line.append(ba.setNum(m_antType[0]));
    line.append(" (fichier .ant); h2 = ");
    line.append(ba.setNum(m_antType[1]));
    line.append(" (fichier .ant)");
    file.write(line);

    line = "\n\nAnt spacing type :\nSpacing type : ";
    line.append(ba.setNum(m_spacingType));
    file.write(line);

    line = "\nBase spacing : ";
    line.append(ba.setNum(m_baseSpacing));
    line.append(" lambda; \tArray design frequency : ");
    line.append(ba.setNum(m_designFreq));
    line.append(" MHz");
    file.write(line);

    line = "\nSpacing unit : ";
    line.append(ba.setNum(m_spacingUnit));
    file.write(line);

    line = "\n\n";
    ba = "Antennas paires :";
    line.append(ba.rightJustified(22, ' '));
    int nbOfAntHalf = m_nbOfAnt/2;
    for (int i=0; i<nbOfAntHalf; i++)
    {
        ba.setNum(i+1);
        ba.prepend("P");
        line.append(ba.rightJustified(8, ' '));
    }
    line.append("\n");
    file.write(line);

    ba = "H buttons state :";
    line = ba.rightJustified(22, ' ');
    for (int i=0; i<nbOfAntHalf; i++)
    {
        ba.setNum(m_heightBtState.at(i));
        line.append(ba.rightJustified(8, ' '));

    }
    line.append("\n");
    file.write(line);

    writeArrayData("Antenna spacing :", m_spacing, 2, file);

    ba = "Antennas labels :";
    line = ba.rightJustified(22, ' ');
    for (int i=0; i<m_nbOfAnt; i++)
    {
        ba.setNum(i+1);
        ba.prepend("A");
        line.append(ba.rightJustified(8, ' '));
    }
    line.append("\n");
    file.write(line);

    ba = "Antennas labels :";
    line = ba.rightJustified(22, ' ');
    for (int i=0; i<nbOfAntHalf; i++)
    {
        ba.setNum(nbOfAntHalf-i);
        ba.prepend("R");
        line.append(ba.rightJustified(8, ' '));
    }

    if (m_nbOfAnt%2!=0)
    {
        ba = "A0";
        line.append(ba.rightJustified(8, ' '));
    }

    for (int i=0; i<nbOfAntHalf; i++)
    {
        ba.setNum(i+1);
        ba.prepend("L");
        line.append(ba.rightJustified(8, ' '));
    }
    line.append("\n\n");
    file.write(line);

    writeArrayData("Ant pos X error (m) :", m_antPosErrorX, 3, file);
    writeArrayData("Ant pos Y error (m) :", m_antPosErrorY, 3, file);
    writeArrayData("Ant pos Z error (m) :", m_antPosErrorZ, 3, file);

    line = "\n";
    file.write(line);
    writeSingleData("Array rotation (°) :", m_arrayRot, 23, 8, 3, file);
    writeSingleData("Array Y offset (m) :", m_arrayY_Offset, 22, 8, 3, file);

    line = "\n";
    file.write(line);
    writeArrayData("CSB CRS Amp :", m_ampCSB_CRS, 4, file);
    writeArrayData("CSB CRS Phase :", m_phCSB_CRS, 1, file);
    writeArrayData("SBO CRS Amp :", m_ampSBO_CRS, 4, file);
    writeArrayData("SBO CRS Phase :", m_phSBO_CRS, 1, file);

    writeArrayData("CSB CLEAR Amp :", m_ampCSB_CLR, 4, file);
    writeArrayData("CSB CLEAR Phase :", m_phCSB_CLR, 1, file);
    writeArrayData("SBO CLEAR Amp :", m_ampSBO_CLR, 4, file);
    writeArrayData("SBO CLEAR Phase :", m_phSBO_CLR, 1, file);

    line = "\n";
    file.write(line);
    writeArrayData("CSB CLEAR 2 Amp :", m_ampCSB_CLR2, 4, file);
    writeArrayData("CSB CLEAR 2 Phase :", m_phCSB_CLR2, 1, file);
    writeArrayData("SBO CLEAR 2 Amp :", m_ampSBO_CLR2, 4, file);
    writeArrayData("SBO CLEAR 2 Phase :", m_phSBO_CLR2, 1, file);

    line = "\n";
    file.write(line);
    ba = "CLR2_On : ";
    line = ba.rightJustified(22, ' ');
    line.append(ba.setNum(m_CLR2_On).rightJustified(8, ' '));
    file.write(line);

    line = "\n\n";
    file.write(line);
    writeArrayData("Ant Error Amp :", m_ampAntError, 4, file);
    writeArrayData("Ant Error Phase :", m_phAntError, 1, file);
    writeArrayData("Coupling Amp :", m_ampCoupling, 4, file);
    writeArrayData("Coupling Phase :", m_phCoupling, 1, file);

    line = "\nAssociated Electronic :\n";
    file.write(line);
    writeSingleData("Default Signal Generator :", m_defaultSigGen, 28, 4, file, " (NM7000B)");
    writeSingleData("Default Course power :", m_defaultCRS_Power, 28, 8, 2, file, " W");
    writeSingleData("Default Clear power :", m_defaultCLR_Power, 28, 8, 2, file, " W");
    writeSingleData("Clear/Course ratio on CL :", m_CLR_CRS_Ratio, 28, 8, 2, file);
    writeSingleData("Azimut Clearance :", m_CLR_Az, 28, 8, 2, file, "°");
    writeSingleData("DDM Clearance at Az :", m_CLR_DDMatAz, 28, 8, 2, file);

    line = "\nPhoto :\n";
    file.write(line);
    writeSingleData("Index :", m_photoIndex, 8, 2, file);

    file.close();
    return true;
}

void AntennaArray::readArrayData(QString &text, QString label, int &indexEnd, QVector<int> *array)
{
    int indexLineLabel = text.indexOf(label, indexEnd);
    if (indexLineLabel==-1)
    {
        qWarning() << "Label " << label << " not found after index " << indexEnd;
    }
    else
    {
        int indexBegin = text.indexOf(":", indexLineLabel) + 1;
        indexEnd = text.indexOf("\n", indexBegin);
        QString text2 = text.mid(indexBegin, indexEnd-indexBegin).trimmed();
        QStringList textList = text2.split("\t");
        foreach (QString str, textList)
            array->append(str.toDouble());
    }
}

void AntennaArray::readArrayData(QString &text, QString label, int &indexEnd, QVector<double> *array)
{
    int indexLineLabel = text.indexOf(label, indexEnd);
    if (indexLineLabel==-1)
    {
        qWarning() << "Label " << label << " not found after index " << indexEnd;
    }
    else
    {
        int indexBegin = text.indexOf(":", indexLineLabel) + 1;
        indexEnd = text.indexOf("\n", indexBegin);
        QString text2 = text.mid(indexBegin, indexEnd-indexBegin).trimmed();
        QStringList textList = text2.split("\t");
        array->clear();
        foreach (QString str, textList)
            array->append(str.toDouble());
    }

}

void AntennaArray::readSingleData(QString &text, QString label, QString endChar, int &indexEnd, int &data)
{
    int indexLineLabel = text.indexOf(label, indexEnd);
    if (indexLineLabel==-1)
    {
        qWarning() << "Label " << label << " not found after index " << indexEnd;

    }
    else
    {
        int indexBegin = text.indexOf(":", indexLineLabel) + 1;
        indexEnd = text.indexOf(endChar, indexBegin);
        QString text2 = text.mid(indexBegin, indexEnd-indexBegin).trimmed();
        data = text2.toInt();
    }

}

void AntennaArray::readSingleData(QString &text, QString label, QString endChar, int &indexEnd, double &data)
{
    int indexLineLabel = text.indexOf(label, indexEnd);
    if (indexLineLabel==-1)
    {
        qWarning() << "Label " << label << " not found after index " << indexEnd;

    }
    else
    {
        int indexBegin = text.indexOf(":", indexLineLabel) + 1;
        indexEnd = text.indexOf(endChar, indexBegin);
        QString text2 = text.mid(indexBegin, indexEnd-indexBegin).trimmed();
        data = text2.toDouble();
    }

}

void AntennaArray::writeSingleData(QByteArray ba, double &var, int labelWidth, int dataWidth,
                                   int nbOfDeci, QFile &file, QByteArray endText)
{
    QByteArray line = ba.rightJustified(labelWidth, ' ');
    ba.setNum(var, 'f', nbOfDeci);//Formattage avec nbOfDeci chiffres après la virgule
    line.append(ba.rightJustified(dataWidth, ' '));
    line.append(endText);
    line.append("\n");
    file.write(line);
}

void AntennaArray::writeSingleData(QByteArray ba, int &var,
                                   int labelWidth, int dataWidth, QFile &file, QByteArray endText)
{
    QByteArray line = ba.rightJustified(labelWidth, ' ');
    ba.setNum(var);
    line.append(ba.rightJustified(dataWidth, ' '));
    line.append(endText);
    line.append("\n");
    file.write(line);
}

void AntennaArray::writeArrayData(QByteArray ba, QVector<double> &array, int nbOfDeci, QFile &file)
{
    QByteArray line = ba.rightJustified(22, ' ');
    foreach(double var, array)
    {
        ba.setNum(var, 'f', nbOfDeci);//Formattage avec nbOfDeci chiffres après la virgule
        line.append(ba.rightJustified(8, ' '));
    }
    line.append("\n");
    file.write(line);
}




